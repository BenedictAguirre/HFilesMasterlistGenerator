package com.svi.hfiles.masterlistgenerator.objects;

import java.util.List;

public class Employee {
	private String employeeNo;
	private String elementsFolderPath;
	private List<XmlFile> xmlFiles;
	public Employee(String employeeNo, String elementsFolderPath) {
		this.employeeNo = employeeNo;
		this.elementsFolderPath = elementsFolderPath;
	}
	public String getEmployeeNo() {
		return employeeNo;
	}
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}
	public List<XmlFile> getXmlFiles() {
		return xmlFiles;
	}
	public void setXmlFiles(List<XmlFile> xmlFiles) {
		this.xmlFiles = xmlFiles;
	}
	public String getElementsFolderPath() {
		return elementsFolderPath;
	}
	public void setElementsFolderPath(String elementsFolderPath) {
		this.elementsFolderPath = elementsFolderPath;
	}
	

}
