package com.svi.hfiles.masterlistgenerator.objects;

import java.util.List;

public class XmlFile {
	private List<XmlData> xmlFile;
	private String documentType;
	public XmlFile(String documentType, List<XmlData> xmlFile) {
		this.xmlFile = xmlFile;
		this.documentType = documentType;
	}

	public List<XmlData> getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(List<XmlData> xmlFile) {
		this.xmlFile = xmlFile;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
}
