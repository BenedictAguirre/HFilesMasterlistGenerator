package com.svi.hfiles.masterlistgenerator.objects;

import java.util.List;

public class Field {
	private List<String> fieldNames;
	private String field;
	public Field(List<String> fieldNames, String field) {
		this.field = field;
		this.fieldNames = fieldNames;
	}
	public List<String> getFieldNames() {
		return fieldNames;
	}
	public void setFieldNames(List<String> fieldNames) {
		this.fieldNames = fieldNames;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	
}
