package com.svi.hfiles.masterlistgenerator.objects;

public class DataMap {
	private String columnId;
	private String columnHeader;
	private String xmlLabel;

	public DataMap(String columnId, String columnHeader, String xmlLabel) {
		this.columnId = columnId;
		this.columnHeader = columnHeader;
		this.xmlLabel = xmlLabel;
	}
	public String getColumnId() {
		return columnId;
	}
	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}
	public String getColumnHeader() {
		return columnHeader;
	}
	public void setColumnHeader(String columnHeader) {
		this.columnHeader = columnHeader;
	}
	public String getXmlLabel() {
		return xmlLabel;
	}
	public void setXmlLabel(String xmlLabel) {
		this.xmlLabel = xmlLabel;
	}


}
