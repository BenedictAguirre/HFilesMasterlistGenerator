package com.svi.hfiles.masterlistgenerator.objects;

public class XmlData {
	private String index;
	private String xmlField;
	private String xmlValue;
	public XmlData( String xmlField, String xmlValue) {
//		this.index = index;
		this.xmlField = xmlField;
		this.xmlValue = xmlValue;
	}
	public XmlData(String index, String xmlField, String xmlValue) {
		this.index = index;
		this.xmlField = xmlField;
		this.xmlValue = xmlValue;
	}
	public String getXmlField() {
		return xmlField;
	}
	public void setXmlField(String xmlField) {
		this.xmlField = xmlField;
	}
	public String getXmlValue() {
		return xmlValue;
	}
	public void setXmlValue(String xmlValue) {
		this.xmlValue = xmlValue;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
}
