package com.svi.hfiles.masterlistgenerator.csvwriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.svi.hfiles.masterlistgenerator.config.Config;
import com.svi.hfiles.masterlistgenerator.excelparser.ExcelReader;
import com.svi.hfiles.masterlistgenerator.fileprocessor.SpecialConditionHandler;
import com.svi.hfiles.masterlistgenerator.fileprocessor.XmlDataCollector;
import com.svi.hfiles.masterlistgenerator.objects.DataMap;
import com.svi.hfiles.masterlistgenerator.objects.Employee;
import com.svi.hfiles.masterlistgenerator.objects.XmlData;
import com.svi.hfiles.masterlistgenerator.objects.XmlFile;

public class CsvWriter {
	ExcelReader reader = new ExcelReader();
	SpecialConditionHandler handler = new SpecialConditionHandler();
	XmlDataCollector col = new XmlDataCollector();

	public String getEmployeeDataString(Employee employee, String fieldMapPath) {
		List<DataMap> columnXmlMap = reader.getColumnXmlMap(fieldMapPath);
		List<String> finalData = new ArrayList<String>();
		for (DataMap map : columnXmlMap) {
			finalData.add("");
		}

		XmlFile main = col.getMainXmlFiles(employee.getXmlFiles(), Config.getProperty("mainXmlFile"));
		for (XmlData data : main.getXmlFile()) {
			finalData.set(reader.getColumnIndex(data.getIndex()), data.getXmlValue());
		}
		for (XmlFile file : employee.getXmlFiles()) {
			for (XmlData data : file.getXmlFile()) {
				if (finalData.get(reader.getColumnIndex(data.getIndex())).equals("")) {
					finalData.set(reader.getColumnIndex(data.getIndex()), data.getXmlValue());
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		for (String string : finalData) {
			sb.append(string + "|");
		}
		String finalString = sb.toString();
		if (finalString.endsWith("|")) {
			finalString = finalString.substring(0, finalString.length() - 1) + "\n";
		}
		return finalString;
	}

	public void appendToMasterlist(Employee employee, String fieldMapPath, String masterlistFilePath) {
		List<DataMap> datamap = reader.getColumnXmlMap(fieldMapPath);
		String employeeDataString = getEmployeeDataString(employee, fieldMapPath);
		FileWriter masterlist;
		try {
			masterlist = new FileWriter(masterlistFilePath, true);
			File mfile = new File(masterlistFilePath);
			if (mfile.length() == 0) {
				for (int i = 0; i < datamap.size(); i++) {
					masterlist.append(datamap.get(i).getColumnHeader());
					if (i == datamap.size() - 1) {
						masterlist.append("\n");
					} else {
						masterlist.append("|");
					}
				}
			}
			masterlist.append(employeeDataString);
			System.out.println("Data " + employeeDataString + "added to " + masterlistFilePath);
			masterlist.flush();
			masterlist.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
