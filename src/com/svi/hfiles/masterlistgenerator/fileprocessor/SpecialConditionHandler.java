package com.svi.hfiles.masterlistgenerator.fileprocessor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.svi.hfiles.masterlistgenerator.config.Config;
import com.svi.hfiles.masterlistgenerator.constants.Companies;
import com.svi.hfiles.masterlistgenerator.constants.CompanyIds;
import com.svi.hfiles.masterlistgenerator.constants.Requirements;
import com.svi.hfiles.masterlistgenerator.constants.SpecialFields;
import com.svi.hfiles.masterlistgenerator.excelparser.ExcelReader;
import com.svi.hfiles.masterlistgenerator.objects.DataMap;
import com.svi.hfiles.masterlistgenerator.objects.XmlData;

public class SpecialConditionHandler {
	private static final String TRAINING_AGREEMENT = Requirements.TRAINING_AGREEMENT.getString();
	private static final String EMPLOYMENT_CONTRACT = Requirements.EMPLOYMENT_CONTRACT.getString();
	private static final String AGREEMENT_FOR_PROJECT_EMPLOYMENT = Requirements.AGGREEMENT_FOR_PROJECT_EMPLOYMENT
			.getString();
	private static final String CONTRACT_FOR_PROBATIONARY_EMPLOYMENT = Requirements.CONTRACT_FOR_PROBATIONARY_EMPLOYMENT
			.getString();
	private static final String INPUT_PROCESSING = "input processing";
	private static final String DAILY = "Daily";
	private static final String MONTHLY = "Monthly";
	private static final String SSC = Companies.SSC.getString();
	private static final String ISC = Companies.ISC.getString();
	private static final String SVIC = Companies.SVIC.getString();
	private static final String SVIT = Companies.SVIT.getString();
	private static final String CDCI = Companies.CDCI.getString();
	private static final String DATE_FORMAT1 = "MM/dd/yyyy";
	private static final String DATE_FORMAT2 = "dd-MM-yyyy";
	private static final String TRAINEE_INDICATOR = "Trainee";
	private static final String ACTIVE_INDICATOR = "Active";

	ExcelReader excelReader = new ExcelReader();
	List<DataMap> dataMap = excelReader.getColumnXmlMap(Config.getProperty("fieldMapExcelFile"));

	private boolean checkIfEmploymentStatusFieldExists(List<XmlData> xmlFile) {
		boolean exists = false;
		for (XmlData data : xmlFile) {
			if (data.getXmlField().equals(SpecialFields.EMP_STATUS.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	private boolean checkIfEmploymentTypeFieldExists(List<XmlData> xmlFile) {
		boolean exists = false;
		for (XmlData data : xmlFile) {
			if (data.getXmlField().equals(SpecialFields.EMP_TYPE.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	public void applyConditionForEmploymentStatus(List<XmlData> xmlFile, List<String> docTypes) {
		if (docTypes.contains(TRAINING_AGREEMENT) && !docTypes.contains(AGREEMENT_FOR_PROJECT_EMPLOYMENT)
				&& !docTypes.contains(EMPLOYMENT_CONTRACT)
				&& !docTypes.contains(CONTRACT_FOR_PROBATIONARY_EMPLOYMENT)) {
			for (XmlData data : xmlFile) {
				if (checkIfEmploymentStatusFieldExists(xmlFile)) {
					if (data.getXmlField().equals(SpecialFields.EMP_STATUS.getString())) {
						data.setXmlValue(TRAINEE_INDICATOR);
					}
				} else {
					for (DataMap map : dataMap) {
						if (map.getXmlLabel().equals(SpecialFields.EMP_STATUS.getString())) {
							xmlFile.add(new XmlData(map.getColumnId(), SpecialFields.EMP_STATUS.getString(),
									TRAINEE_INDICATOR));
						}
					}
				}
				if (checkIfEmploymentTypeFieldExists(xmlFile)) {
					if (data.getXmlField().equals(SpecialFields.EMP_TYPE.getString())) {
						data.setXmlValue(TRAINEE_INDICATOR);
					}
				} else {
					for (DataMap map : dataMap) {
						if (map.getXmlLabel().equals(SpecialFields.EMP_TYPE.getString())) {
							xmlFile.add(new XmlData(map.getColumnId(), SpecialFields.EMP_TYPE.getString(),
									TRAINEE_INDICATOR));
						}
					}
				}

			}
		} else if (docTypes.contains(TRAINING_AGREEMENT) && docTypes.contains(AGREEMENT_FOR_PROJECT_EMPLOYMENT)
				&& !docTypes.contains(EMPLOYMENT_CONTRACT)
				&& !docTypes.contains(CONTRACT_FOR_PROBATIONARY_EMPLOYMENT)) {
			if (checkIfEmploymentStatusFieldExists(xmlFile)) {
				for (XmlData data : xmlFile) {
					if (data.getXmlField().equals(SpecialFields.EMP_STATUS.getString())) {
						data.setXmlValue(ACTIVE_INDICATOR);
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.EMP_STATUS.getString())) {
						xmlFile.add(
								new XmlData(map.getColumnId(), SpecialFields.EMP_STATUS.getString(), ACTIVE_INDICATOR));
					}
				}
			}
		} else if ((docTypes.contains(TRAINING_AGREEMENT) || docTypes.contains(EMPLOYMENT_CONTRACT))
				&& docTypes.contains(CONTRACT_FOR_PROBATIONARY_EMPLOYMENT)) {
			if (checkIfEmploymentStatusFieldExists(xmlFile)) {
				for (XmlData data : xmlFile) {
					if (data.getXmlField().equals(SpecialFields.EMP_STATUS.getString())) {
						data.setXmlValue(ACTIVE_INDICATOR);
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.EMP_STATUS.getString())) {
						xmlFile.add(
								new XmlData(map.getColumnId(), SpecialFields.EMP_STATUS.getString(), ACTIVE_INDICATOR));
					}
				}
			}
		}

	}

	private boolean checkIfCompanyIdFieldExists(List<XmlData> xmlData) {
		boolean exists = false;
		for (XmlData data : xmlData) {
			if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	public void applyConditionForCompanyId(List<XmlData> xmlData, String company, String department) {

		if (company != null && department != null && company.equals(SSC)
				&& department.toLowerCase().equals(INPUT_PROCESSING)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.SSC_IPD.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.SSC_IPD.getString()));
					}
				}
			}
		}
		if (company != null && department != null && company.equals(ISC)
				&& department.toLowerCase().equals(INPUT_PROCESSING)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.ISC_IPD.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.ISC_IPD.getString()));
					}
				}
			}
		}

		if (company != null && department != null && company.equals(SSC)
				&& !department.toLowerCase().equals(INPUT_PROCESSING)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.SVI_SSC.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.SVI_SSC.getString()));
					}
				}
			}
		} else if (company != null && department != null && company.equals(ISC)
				&& !department.toLowerCase().equals(INPUT_PROCESSING)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.ISC_MANILA.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.ISC_MANILA.getString()));
					}
				}
			}
		} else if (company != null && company.equals(SVIC)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.SVIC.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.SVIC.getString()));
					}
				}
			}
		} else if (company != null && company.equals(SVIT)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.SVIT.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.SVIT.getString()));
					}
				}
			}
		} else if (company != null && company.equals(CDCI)) {
			if (checkIfCompanyIdFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.COMPANY_ID.getString())) {
						data.setXmlValue(CompanyIds.CDCI.getString());
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.COMPANY_ID.getString())) {
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.COMPANY_ID.getString(),
								CompanyIds.CDCI.getString()));
					}
				}
			}
		}
	}

	private boolean checkIfSalaryComputationBasisFieldExists(List<XmlData> xmlFile) {
		boolean exists = false;
		for (XmlData data : xmlFile) {
			if (data.getXmlField().equals(SpecialFields.SALARY_COMPUTATION_BASIS.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	public void applyConditionForSalaryComputationBasis(List<XmlData> xmlFile, List<String> docTypes) {
		if ((docTypes.contains(TRAINING_AGREEMENT) && docTypes.contains(AGREEMENT_FOR_PROJECT_EMPLOYMENT))
				&& !docTypes.contains(EMPLOYMENT_CONTRACT)
				&& !docTypes.contains(CONTRACT_FOR_PROBATIONARY_EMPLOYMENT)) {
			if (checkIfSalaryComputationBasisFieldExists(xmlFile)) {
				for (XmlData data : xmlFile) {
					if (data.getXmlField().equals(SpecialFields.SALARY_COMPUTATION_BASIS.getString())) {
						data.setXmlValue(DAILY);
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.SALARY_COMPUTATION_BASIS.getString())) {
						xmlFile.add(new XmlData(map.getColumnId(), SpecialFields.SALARY_COMPUTATION_BASIS.getString(),
								DAILY));
					}
				}
			}
		} else if ((docTypes.contains(TRAINING_AGREEMENT) || docTypes.contains(EMPLOYMENT_CONTRACT))
				&& docTypes.contains(CONTRACT_FOR_PROBATIONARY_EMPLOYMENT)) {
			if (checkIfSalaryComputationBasisFieldExists(xmlFile)) {
				for (XmlData data : xmlFile) {
					if (data.getXmlField().equals(SpecialFields.SALARY_COMPUTATION_BASIS.getString())) {
						data.setXmlValue(MONTHLY);
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.SALARY_COMPUTATION_BASIS.getString())) {
						xmlFile.add(new XmlData(map.getColumnId(), SpecialFields.SALARY_COMPUTATION_BASIS.getString(),
								MONTHLY));
					}
				}
			}
		}
	}

	public static boolean isValidDate1(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT1);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public static boolean isValidDate2(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT2);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public static boolean isValidDate(String inDate) {
		boolean isValid = false;
		if (isValidDate1(inDate) || isValidDate2(inDate)) {
			isValid = true;
		}
		return isValid;

	}

	public void applyConditionForDate(List<XmlData> xmlFile) {
		for (XmlData data : xmlFile) {
			if (isValidDate(data.getXmlValue())) {
				try {
					Date date1 = new SimpleDateFormat(DATE_FORMAT1).parse(data.getXmlValue());
					DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT1);
					data.setXmlValue(dateFormat.format(date1));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private boolean checkIfBasicDailyRateFieldExists(List<XmlData> xmlData) {
		boolean exists = false;
		for (XmlData data : xmlData) {
			if (data.getXmlField().equals(SpecialFields.BASIC_DAILY_RATE.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	private boolean checkIfBasicHourlyRateFieldExists(List<XmlData> xmlData) {
		boolean exists = false;
		for (XmlData data : xmlData) {
			if (data.getXmlField().equals(SpecialFields.BASIC_HOURLY_RATE.getString())) {
				exists = true;
			}
		}
		return exists;
	}

	public void applyConditionForBasicHourlyRate(List<XmlData> xmlData) {
		double dailyRate = 0;
		if (checkIfBasicDailyRateFieldExists(xmlData)) {
			for (XmlData data : xmlData) {
				if (data.getXmlField().equals(SpecialFields.BASIC_DAILY_RATE.getString())) {
					if (!data.getXmlValue().equals("")) {
						dailyRate = Double.parseDouble(data.getXmlValue());
					}
					
				}
			}
		}
		if (dailyRate != 0) {
			if (checkIfBasicHourlyRateFieldExists(xmlData)) {
				for (XmlData data : xmlData) {
					if (data.getXmlField().equals(SpecialFields.BASIC_HOURLY_RATE.getString())) {
						double s = dailyRate/8;
						data.setXmlValue(Double.toString(s));
					}
				}
			} else {
				for (DataMap map : dataMap) {
					if (map.getXmlLabel().equals(SpecialFields.BASIC_HOURLY_RATE.getString())) {
						double s = dailyRate/8;
						xmlData.add(new XmlData(map.getColumnId(), SpecialFields.BASIC_HOURLY_RATE.getString(),
								Double.toString(s)));
					}
				}
			}
		}

	}

	public void applyAllConditions(List<XmlData> xmlFile, List<String> docTypes, String company, String department) {
		applyConditionForBasicHourlyRate(xmlFile);
		applyConditionForCompanyId(xmlFile, company, department);
		applyConditionForEmploymentStatus(xmlFile, docTypes);
		applyConditionForSalaryComputationBasis(xmlFile, docTypes);
	}
}
