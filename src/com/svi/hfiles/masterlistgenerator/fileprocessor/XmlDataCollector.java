package com.svi.hfiles.masterlistgenerator.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.svi.hfiles.masterlistgenerator.constants.SpecialFields;
import com.svi.hfiles.masterlistgenerator.excelparser.ExcelReader;
import com.svi.hfiles.masterlistgenerator.objects.DataMap;
import com.svi.hfiles.masterlistgenerator.objects.Employee;
import com.svi.hfiles.masterlistgenerator.objects.Field;
import com.svi.hfiles.masterlistgenerator.objects.XmlData;
import com.svi.hfiles.masterlistgenerator.objects.XmlFile;
import com.svi.hfiles.masterlistgenerator.xmlparser.XmlReader;

public class XmlDataCollector {
	XmlReader xmlReader = new XmlReader();
	ExcelReader excelReader = new ExcelReader();
	SpecialConditionHandler handler = new SpecialConditionHandler();

	public List<XmlData> getXmlData(String xmlPath) {
		List<XmlData> xmlData = new ArrayList<XmlData>();
		Element root = xmlReader.getDocumentElement(xmlPath);
		xmlReader.normalizeRootElement(root);
		List<Node> nodes = xmlReader.getChildNodes(root);
		for (Node node : nodes) {
//			String index;
			String xmlField;
			String xmlValue;
			Element element = xmlReader.convertNodeToElement(node);
			xmlField = node.getNodeName();
			xmlValue = node.getTextContent();
			xmlData.add(new XmlData(xmlField, xmlValue));
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				List<Node> childNodes = xmlReader.getChildNodes(element);
				for (Node childNode : childNodes) {
					xmlField = childNode.getNodeName();
					xmlValue = childNode.getTextContent();
					xmlData.add(new XmlData(xmlField, xmlValue));
				}
			}

		}

		return xmlData;
	}

	private List<XmlData> filterXmlData(String xmlPath, String fieldMapPath, Employee employee) {
		List<XmlData> xmlFileData = new ArrayList<XmlData>();
		List<Field> fieldFilter = excelReader.getFieldFilter(fieldMapPath);
		List<DataMap> map = excelReader.getColumnXmlMap(fieldMapPath);
		List<XmlData> xmlData = getXmlData(xmlPath);
		handler.applyAllConditions(xmlData, getListOfDocTypesPerEmployee(employee), getCompany(xmlData),
				getDepartment(xmlData));
		for (XmlData xmlData2 : xmlData) {
			for (Field field : fieldFilter) {
				if (field.getFieldNames().contains(xmlData2.getXmlField())) {
					xmlData2.setXmlField(field.getField());
					for (DataMap dataMap : map) {
						if (dataMap.getColumnHeader().equals(field.getField())) {
							if (!xmlData2.getXmlValue().equals("")) {
								xmlFileData.add(
										new XmlData(dataMap.getColumnId(), field.getField(), xmlData2.getXmlValue()));
							}

						}
					}

				}
			}
		}
		return xmlFileData;
	}

	public List<String> getXmlFilePathsFromDirectory(String xmlFolderPath) {
		List<String> allFiles = new ArrayList<>();
		List<String> allXml = new ArrayList<>();
		try (Stream<Path> walk = Files.walk(Paths.get(xmlFolderPath))) {
			allFiles = walk.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}
		for (String string : allFiles) {
			if (string.endsWith(".xml")) {
				allXml.add(string);
			}
		}
		return allXml;
	}

	public List<XmlFile> getXmlFiles(List<String> xmlFilesPath, String fieldMapPath, Employee employee) {
		List<XmlFile> xmlFiles = new ArrayList<>();
		for (String string : xmlFilesPath) {
			File xmlRawFile = new File(string);
			String documentType = xmlRawFile.getName().substring(0, xmlRawFile.getName().indexOf(".") - 10)
					.toLowerCase();
			List<XmlData> xmlData = filterXmlData(string, fieldMapPath, employee);
			handler.applyConditionForDate(xmlData);
			xmlFiles.add(new XmlFile(documentType, xmlData));
		}
		return xmlFiles;
	}

	public String getCompany(List<XmlData> xmlFile) {
		String company = null;
		for (XmlData xmlData : xmlFile) {
			if (xmlData.getXmlField().equals(SpecialFields.COMPANY.getString()) && company == null) {
				company = xmlData.getXmlValue();
			}

		}
		return company;
	}

	public String getDepartment(List<XmlData> xmlFile) {
		String department = null;

		for (XmlData xmlData : xmlFile) {
			if (xmlData.getXmlField().equals(SpecialFields.DEPARTMENT.getString()) && department == null) {
				department = xmlData.getXmlValue();
			}

		}
		return department;
	}

	public XmlFile getMainXmlFiles(List<XmlFile> xmlFiles, String mainXmlFile) {
		XmlFile mainFiles = null;
		for (XmlFile xmlFile : xmlFiles) {
			if (xmlFile.getDocumentType().toUpperCase().contains(mainXmlFile)) {
				mainFiles = xmlFile;
			}
		}
		return mainFiles;
	}

	public List<Employee> getEmployees(String inputPath, String fieldMapPath) {
		List<Employee> employees = new ArrayList<>();
		File dir = new File(inputPath);
		File[] dirs = dir.listFiles();
		for (File string : dirs) {
//			System.out.println(string.getAbsolutePath());
			String employeeNo = string.getName().substring(0, string.getName().indexOf("_"));
//			System.out.println(employeeNo);
			List<String> xmlFilesPath = getXmlFilePathsFromDirectory(string.getAbsolutePath());
			Employee employee = new Employee(employeeNo, string.getAbsolutePath());
			List<XmlFile> xmlFiles = getXmlFiles(xmlFilesPath, fieldMapPath, employee);
			employee.setXmlFiles(xmlFiles);
			employees.add(employee);
		}
		return employees;
	}

	public List<String> getListOfDocTypesPerEmployee(Employee employee) {
		List<String> doctypes = new ArrayList<String>();
		List<String> xmlFilesPath = getXmlFilePathsFromDirectory(employee.getElementsFolderPath());
		for (String string : xmlFilesPath) {
			File file = new File(string);
			String filename = file.getName();
			filename = filename.substring(0, filename.indexOf(".")-10);
			doctypes.add(filename);
		}
		return doctypes;

	}

}
