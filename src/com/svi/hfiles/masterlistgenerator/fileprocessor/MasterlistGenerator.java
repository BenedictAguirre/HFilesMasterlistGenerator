package com.svi.hfiles.masterlistgenerator.fileprocessor;


import java.util.List;

import com.svi.hfiles.masterlistgenerator.config.Config;
import com.svi.hfiles.masterlistgenerator.constants.SpecialFields;
import com.svi.hfiles.masterlistgenerator.csvwriter.CsvWriter;
import com.svi.hfiles.masterlistgenerator.objects.DataMap;
import com.svi.hfiles.masterlistgenerator.objects.Employee;
import com.svi.hfiles.masterlistgenerator.objects.XmlData;

public class MasterlistGenerator {
	private static final String FIELD_MAP_PATH = Config.getProperty("fieldMapExcelFile");
	private static final String MASTERLIST_FILE = Config.getProperty("masterlistExcelFile");
	private static final String INPUT_PATH = Config.getProperty("xmlFolderPath");

	public void writeToMasterlist() {
		XmlDataCollector col = new XmlDataCollector();
		CsvWriter writer = new CsvWriter();
		List<Employee> employees = col.getEmployees(INPUT_PATH, FIELD_MAP_PATH);
		for (Employee employee : employees) {
			writer.appendToMasterlist(employee, FIELD_MAP_PATH, MASTERLIST_FILE);
		}
	}
		

	public static void main(String[] args) {
		MasterlistGenerator generator = new MasterlistGenerator();
		generator.writeToMasterlist();
	
	}

}
