package com.svi.hfiles.masterlistgenerator.xmlparser;

import java.io.File;
import java.io.IOException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;


public class XmlReader {
	public Element getDocumentElement(String fileName) {
		File inputFile = new File(fileName);
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		Document doc;
		Element rootElement = null;

		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(inputFile);
			rootElement = doc.getDocumentElement();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rootElement;
	}

	public void normalizeRootElement(Element rootElement) {
		rootElement.normalize();
	}

	public String getAttribute(Element element, String attributeName) {
		String value = null;
		value = element.getAttribute(attributeName);
		return value;
	}

	public List<Node> getElementsByTagName(Element element, String tagName) {
		List<Node> nodes = new ArrayList<>();;
		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			nodes.add(nodeList.item(i));
		}
		return nodes;
	}

	public List<Node> getChildNodes(Element element) {
		List<Node> nodes = new ArrayList<>();
		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			nodes.add(nodeList.item(i));
		}
		return nodes;
	}

	public String getTextContent(Element element, String tagName) {
		String value = null;
		value = element.getElementsByTagName(tagName).item(0).getTextContent();
		return value;

	}

	public Element convertNodeToElement(Node node) {
		Element element = null;
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			element = (Element) node;
		}
		return element;
	}

	public static List<Node> asList(NodeList n) {
		return n.getLength() == 0 ? Collections.<Node>emptyList() : new NodeListWrapper(n);
	}

	static final class NodeListWrapper extends AbstractList<Node> implements RandomAccess {
		private final NodeList list;

		NodeListWrapper(NodeList l) {
			list = l;
		}

		public Node get(int index) {
			return list.item(index);
		}

		public int size() {
			return list.getLength();
		}
	}
}
