package com.svi.hfiles.masterlistgenerator.constants;

public enum Requirements {
	
	TRAINING_AGREEMENT("training agreement"),
	EMPLOYMENT_CONTRACT("employment contract"),
	AGGREEMENT_FOR_PROJECT_EMPLOYMENT("agreement for project employment"),
	CONTRACT_FOR_PROBATIONARY_EMPLOYMENT("contract of probationary employment"),
	NEW_HIRE_INFORMATION_FORM("new hire information form");

	private String constant;

	public String getString() {
		return this.constant;
	}

	private Requirements(String constant) {
		this.constant = constant;
	}

}
