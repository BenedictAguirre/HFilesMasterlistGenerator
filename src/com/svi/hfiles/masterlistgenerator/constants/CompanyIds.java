package com.svi.hfiles.masterlistgenerator.constants;

public enum CompanyIds {
	SSC_IPD("SSC-IPD"),
	ISC_IPD("ISC-IPD"),
	SVI_SSC("SVI-SSC"),
	ISC_MANILA("ISC Manila"),
	SVIC("SVIC"),
	SVIT("SVIT"),
	CDCI("CDCI");
	private String constant;

	public String getString() {
		return this.constant;
	}

	private CompanyIds(String constant) {
		this.constant = constant;
	}
}


