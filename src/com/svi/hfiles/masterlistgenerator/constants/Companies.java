package com.svi.hfiles.masterlistgenerator.constants;

public enum Companies {
	SSC("SSC"),
	ISC("ISC"),
	SVIC("SVIC"),
	SVIT("SVIT"),
	CDCI("CDCI");
	private String constant;

	public String getString() {
		return this.constant;
	}

	private Companies(String constant) {
		this.constant = constant;
	}
}
