package com.svi.hfiles.masterlistgenerator.constants;

public enum SpecialFields {
	EMP_STATUS("employment_status"),
	EMP_TYPE("employment_type"),
	COMPANY_ID("company_id"),
	SALARY_COMPUTATION_BASIS("salary_computation_basis"),
	EMP_ID("employee_id"),
	DEPARTMENT("department"),
	COMPANY("company"),
	DOC_TYPE("document_type"), 
	BASIC_DAILY_RATE("basic_daily_rate"),
	BASIC_HOURLY_RATE("basic_hourly_rate");
	private String constant;

	public String getString() {
		return this.constant;
	}

	private SpecialFields(String constant) {
		this.constant = constant;
	}


}
