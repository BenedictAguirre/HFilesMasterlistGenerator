package com.svi.hfiles.masterlistgenerator.excelparser;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.svi.hfiles.masterlistgenerator.objects.DataMap;
import com.svi.hfiles.masterlistgenerator.objects.Field;



public class ExcelReader {
	public List<DataMap> getColumnXmlMap(String excelFilePath) {
		List<DataMap> dataMaps = new ArrayList<>();
		HSSFWorkbook wBook;
		try {
			wBook = new HSSFWorkbook(new FileInputStream(excelFilePath));
				HSSFSheet sheet = wBook.getSheetAt(0);
				for (Row row : sheet) {
					String columnId = null;
					String columnHeader = null;
					String xmlLabel = null;
					for (Cell cell : row) {
						if (cell.getColumnIndex()==0) {
							columnId = cell.getStringCellValue();
						}
						if (cell.getColumnIndex()==1) {
							columnHeader = cell.getStringCellValue();
						}
						if (cell.getColumnIndex()==2) {
							xmlLabel = cell.getStringCellValue();
						}
					}
					dataMaps.add(new DataMap(columnId, columnHeader, xmlLabel));
				}
			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dataMaps;
	}
	
	public List<Field> getFieldFilter(String excelFilePath) {
		List<Field> fieldFilter = new ArrayList<>();
		HSSFWorkbook wBook;
		try {
			wBook = new HSSFWorkbook(new FileInputStream(excelFilePath));
				HSSFSheet sheet = wBook.getSheetAt(1);
				for (Row row : sheet) {
					String[] fieldnames = null;
					List<String> fnames = new ArrayList<String>();
					String field = null;
					for (Cell cell : row) {
						if (cell.getColumnIndex()==0) {
							String cellValue = cell.getStringCellValue();
							fieldnames = cellValue.split(",");							
						}
						if (cell.getColumnIndex()==1) {
							field = cell.getStringCellValue();
						}
					}
					for (String string : fieldnames) {
						fnames.add(string.trim());
					}
					fieldFilter.add(new Field(fnames, field));
				}
			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fieldFilter;
	}
	
	public int getColumnIndex(String index) {
		int result = 0;
		for (int i = 0; i < index.length(); i++) 
	    { 
	        result *= 26; 
	        result += index.charAt(i) - 'A' + 1; 
	    } 
		return result-=1;
	}
}
